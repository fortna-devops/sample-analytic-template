#!/usr/bin/env python3
"""
The entry point of the lambda function.
"""

from datetime import datetime
from datetime import timedelta
import math
import boto3
from boto3.dynamodb.conditions import Key, Attr
import logging
import json
import time
import os
from datetime import timezone
from decimal import Decimal

logger = logging.getLogger()
logger.setLevel(logging.INFO)

LOCALTEST = True

FILTER_INTERVAL=1 #Days - SHOULD MATCH CLOUDWATCH RATE

ddb = boto3.client('dynamodb', region_name='us-east-1',
                  aws_access_key_id="AKIAJTPB5VVXGZT34HJA",
                  aws_secret_access_key="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",
                  endpoint_url="https://dynamodb.us-east-1.amazonaws.com/")
dynamodb = boto3.resource('dynamodb',
                         region_name='us-east-1',
                         aws_access_key_id="AKIAJTPB5VVXGZT34HJA",
                         aws_secret_access_key="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",
                         endpoint_url="https://dynamodb.us-east-1.amazonaws.com/")


def vvrms_score(vvrms,equipment):
    if equipment == 'B':
        return 0
    elif vvrms < 0.04:
        return 0
    elif vvrms < 0.11:
        return 1
    elif vvrms < 0.28:
        return 2
    else: 
        return 3
        
def temp_score(temp):
    if temp < 150:
        return 0
    elif temp < 170:
        return 2
    else: 
        return 3
        
def standards_score(vvrms_score,temp_score):
    return max([vvrms_score,temp_score])


def lambda_handler(event, context):
    if LOCALTEST:
        site = '1'
        TZ_OFFSET = -4
        CUST = '1'

    else:
        site=str(event.get('site'))
        TZ_OFFSET=int(os.getenv('TZ_OFFSET'))
        CUST=os.getenv('CUST')
    
    iso_metric = 'X_RMS_V_IPS'
    iso_metric2='Z_RMS_IPS'

    if  LOCALTEST:
        global dynamodb
    else:
        dynamodb = boto3.resource('dynamodb')
    raw_table = dynamodb.Table('Site'+site)
    stats_table = dynamodb.Table('Stats_Cust'+CUST)
    location_table = dynamodb.Table('Sensor_Locations')
    
    # get all sensors
    response = location_table.query(
        KeyConditionExpression=Key('site').eq(site)
        )
    equipment=response['Items']
    
    all_data=[]
    
    for e in equipment:
        
        sensor=e.get('sensor')
         #for each sensor, get the last FILTER_INTERVAL minutes of data
        nw=datetime.now(timezone(timedelta(hours=TZ_OFFSET))).strftime("%Y-%m-%dT%H:%M")
        before=(datetime.now(timezone(timedelta(hours=TZ_OFFSET))) - timedelta(days=FILTER_INTERVAL)).strftime("%Y-%m-%dT%H:%M")
        try:
            response = raw_table.query(
                    KeyConditionExpression=Key('hash_key').eq(e.get('hash_key')) & Key('read_time').between(before,nw),
                    ProjectionExpression=iso_metric+','+iso_metric2,
                    FilterExpression=Attr('conveyor_on').eq(1)
            )
            items=response['Items']
        
            all_data=e
            
            #create empty list for threshold computation
            metrics=list()
            # get the data to compute the threshold
            for data in items:
                for d in data:
                    if data.get(iso_metric) is not None:
                            metrics.append(data.get(iso_metric))
    
            
            #compute mode
            if (len(metrics)>0):
                ds=sorted(metrics)
                d1mode=max(set(ds), key=ds.count) #should be peak of baseline
                d1mode_index=len(ds) - 1 - ds[::-1].index(d1mode)#index of last d1mode value
                if d1mode_index == len(ds)-1:
                    d1mode_index -= 1
                d2=ds[(d1mode_index+1):len(ds)]
                d2=d2[math.floor(len(d2)*0.5):len(d2)]
                d2mode=max(set(d2), key=d2.count)
            
                p90=metrics[math.floor(len(metrics)*0.9)]
        
                all_data[iso_metric+'_mode']=d2mode
                all_data[iso_metric+'_p90']=p90
                if e.get('equipment')=='B':
                    all_data[iso_metric+'_mode2']=-1
                    all_data[iso_metric+'_p90_2']=-1
                else:
                    all_data[iso_metric+'_mode2']=d2mode
                    all_data[iso_metric+'_p90_2']=p90
                    
                
                #create empty list for threshold computation
                metrics=list()
                # get the data to compute the threshold
                for data in items:
                    for d in data:
                        if data.get(iso_metric2) is not None:
                                metrics.append(data.get(iso_metric2))
            
                #compute mode
           
                ds=sorted(metrics)
                d1mode=max(set(ds), key=ds.count) #should be peak of baseline
                d1mode_index=len(ds) - 1 - ds[::-1].index(d1mode)#index of last d1mode value
                if d1mode_index == len(ds)-1:
                    d1mode_index -= 1
                d2=ds[(d1mode_index+1):len(ds)]
                d2=d2[math.floor(len(d2)*0.5):len(d2)]
                
                d2mode=max(set(d2), key=d2.count)
            
                p90=metrics[math.floor(len(metrics)*0.9)]
        
                all_data[iso_metric2+'_mode']=d2mode
                all_data[iso_metric2+'_p90']=p90
                if e.get('equipment')=='B':
                    all_data[iso_metric2+'_mode2']=-1
                    all_data[iso_metric2+'_p90_2']=-1
                else:
                    all_data[iso_metric2+'_mode2']=d2mode
                    all_data[iso_metric2+'_p90_2']=p90
                    
            
        
            # save data for each sensor to location table
            if len(all_data)>0:
                if len(metrics)>0:
                    all_data['vvrms_score']=vvrms_score(sum(metrics)/len(metrics),all_data['equipment'])
                    
                    #temperature
                    proj_exp='sensor,TEMP_F_perc95'
                    response = stats_table.query(
                            KeyConditionExpression=Key('site').eq(site) & Key('sensor').eq(sensor),
                            ProjectionExpression=proj_exp,
                        )
                    if len(response['Items']) > 0:        
                      item=response['Items'][0]
                    
                      all_data['temp_score']=0
                      if 'TEMP_F_perc95' in item:
                        all_data['temp_score']=temp_score(item['TEMP_F_perc95'])
                      all_data['standards_score']=standards_score(all_data['vvrms_score'],all_data['temp_score'])
                            
                else:
                    all_data['vvrms_score']=0
                    all_data['temp_score']=0
                    all_data['standards_score']=0
                  
                
                if LOCALTEST:
                    logger.debug("All data computed: \n{data}".format(data=all_data))
                else:
                    response = location_table.put_item(
                        Item=all_data
                    )
                
        except Exception as err:
            logger.info(e.get('sensor'))
            logger.info(site)
            logger.info(err)
            #break
            
    return 'Success'



lambda_handler(1,1)