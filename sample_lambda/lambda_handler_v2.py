#!/usr/bin/env python3
"""
The entry point of the lambda function.
"""

# standard python imports
from datetime import datetime, timedelta, timezone
import math
from decimal import Decimal
import logging
import json
import time
import os
# AWS python SDK
import boto3
from boto3.dynamodb.conditions import Key, Attr
# MHS insights analytic toolbox imports
from analytics_toolbox.framework_interfaces import AnalyticEngine



logger = logging.getLogger('analytic-name')
logger.setLevel(logging.INFO)


def lambda_handler(event, context):
	# Instantiate AnalyticEngine class
	smp_analytic_engine = AnalyticEngine()
	# query data from athena
	data_df = smp_analytic_engine.query_data(event.get('start_time'),
								   event.get('end_time'),
								   event.get('tag_list'))

	# create tag container object 
	tags = TagContainer(data_df)

	# use analytic standard implementation to run files in kpi_calcs folder
	smp_analytic_engine.run_kpi_calculations()
	


