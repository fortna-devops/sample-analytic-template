#!/usr/bin/env python3
"""
Sample implementation of an alarm calculation
based on a vibrations threshold set in a configuration file
or database.
"""

class VibrationIps(KeyPerformanceIndicatorCalculation):

	def __init__(self):
		pass

	def run_calculation(tags):

		over_thld = tags.X_RMS_V_IPS > context.get('X_RMS_V_IPS_threshold')

		return over_thld

	def tags_used(tags):
		return tags.X_RMS_V_IPS

