# sample-analytic-template

A sample implementation of an analytic using proposed framework for standardization. Design goals include readability, maintainability, and extensibility.

The final intention is to use this as the starting point for all new analytic development.